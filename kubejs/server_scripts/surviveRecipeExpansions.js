// The bottling machine is stupid and doesn't do things normally
onEvent('server.datapack.high_priority', datapack => {
    function addBottlingRecipe(name, result, input, fluid, fluidAmount) {
        datapack.addJson('immersiveengineering:recipes/bottling/' + name + '.json', {
            type: "immersiveengineering:bottling_machine",
            result: { "item": result },
            input: { "item": input },
            fluid: {
                tag: fluid,
                amount: fluidAmount
            }
        });
    }

    addBottlingRecipe("purified_water", "survive:purified_water_bottle", "minecraft:glass_bottle", "boisreborn:purified_water", 250);
});

onEvent('tags.fluids', tags => {
    tags.remove("minecraft:water", ["survive:purified_water", "survive:flowing_purified_water"]);
    tags.add("boisreborn:purified_water", ["survive:purified_water", "survive:flowing_purified_water"]);
});

onEvent('recipes', recipes => {
    recipes.remove({ id: "survive:purified_water_bottle_cooking" });
    recipes.smoking("survive:purified_water_bottle", Item.of('minecraft:potion', '{Potion:"minecraft:water"}'));
    recipes.campfireCooking("survive:purified_water_bottle", Item.of('minecraft:potion', '{Potion:"minecraft:water"}'));
});