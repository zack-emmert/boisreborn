onEvent("recipes", recipes => {

    function addIECrushingRecipe(input, output, secondaries) {
        if (secondaries != null) {
            recipes.custom({
                "type": "immersiveengineering:crusher",
                "secondaries": secondaries,
                "result": Item.of(output),
                "input": Item.of(input),
                "energy": 1600
            });
        } else {
            recipes.custom({
                "type": "immersiveengineering:crusher",
                "result": Item.of(output),
                "input": Item.of(input),
                "energy": 1600
            });
        }
    }

    function addCreateCrushingRecipe(outputs, input) {
        recipes.custom({
            "type": "create:crushing",
            "ingredients": [Item.of(input)],
            "results": outputs,
            "processingTime": 150
        });
    }

    addIECrushingRecipe("occultism:raw_iesnium", "occultism:iesnium_dust", [{ output: Item.of("occultism:iesnium_dust"), chance: 0.33 }]);
    addIECrushingRecipe("occultism:iesnium_ore", "2x occultism:iesnium_dust");
    addIECrushingRecipe("occultism:iesnium_ingot", "occultism:iesnium_dust");

    addCreateCrushingRecipe([Item.of("occultism:iesnium_dust"), Item.of("occultism:iesnium_dust").withChance(0.33)], "occultism:raw_iesnium");
    addCreateCrushingRecipe([Item.of("2x occultism:iesnium_dust")], "occultism:iesnium_ore");
    addCreateCrushingRecipe([Item.of("occultism:iesnium_dust")], "occultism:iesnium_ingot");

    recipes.remove({ type: "occultism:crushing" });
});