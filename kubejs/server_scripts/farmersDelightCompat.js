settings.logAddedRecipes = true;

onEvent('recipes', event => {
    function addCookingPotRecipe(output, ingredients, container) {
        if (container != null) {
            event.custom({
                type: "farmersdelight:cooking",
                ingredients: ingredients,
                result: output,
                container: container,
                cookingtime: 200
            });
        } else {
            event.custom({
                type: "farmersdelight:cooking",
                ingredients: ingredients,
                result: output,
                cookingtime: 200
            });
        }
    }

    function addCuttingBoardRecipe(outputs, inputs) {
        event.custom({
            "type": "farmersdelight:cutting",
            "ingredients": inputs,
            "tool": {
                "tag": "forge:tools/knives"
            },
            "result": outputs,
        });
    }

    // Move appropriate XercaMod food items to the Farmer's Delight pot
    event.remove({ output: "xercamod:oyakodon" });
    addCookingPotRecipe(Item.of("xercamod:oyakodon"), [
        Ingredient.of("#forge:crops/rice"),
        Ingredient.of("#forge:raw_chicken"),
        Ingredient.of("#forge:eggs")
    ],
        Item.of("minecraft:bowl"));

    event.remove({ output: "xercamod:beef_donburi" });
    addCookingPotRecipe(Item.of("xercamod:beef_donburi"), [
        Ingredient.of("#forge:crops/rice"),
        Ingredient.of("#forge:raw_beef"),
    ],
        Item.of("minecraft:bowl"));

    event.remove({ output: "xercamod:squid_ink_paella" });
    addCookingPotRecipe(Item.of("xercamod:squid_ink_paella"), [
        Ingredient.of("#forge:crops/rice"),
        Ingredient.of("#forge:raw_fishes/cod"),
        Ingredient.of("minecraft:ink_sac"),
        Ingredient.of("minecraft:kelp"),
    ],
        Item.of("minecraft:bowl"));

    event.remove({ output: "xercamod:glow_squid_ink_paella" });
    addCookingPotRecipe(Item.of("xercamod:glow_squid_ink_paella"), [
        Ingredient.of("#forge:crops/rice"),
        Ingredient.of("#forge:raw_fishes/cod"),
        Ingredient.of("minecraft:glow_ink_sac"),
        Ingredient.of("minecraft:kelp"),
    ],
        Item.of("minecraft:bowl"));

    // Various items are now made with dough, flour, or tortillas, not wheat
    let wheatDoughSwaps = [
        "minecraft:cookie",
        "farmersdelight:sweet_berry_cookie",
        "farmersdelight:honey_cookie",
        "farmersdelight:raw_pasta",
        "xercamod:sweet_berry_pie",
        "xercamod:item_donut",
        "minecraft:cake",
        "blue_skies:cherry_pie",
        "xercamod:item_apple_pie",
        "xercamod:ender_cupcake",
        "xercamod:item_pumpkin_cupcake",
        "supplementaries:pancake",
        "xercamod:item_carrot_cupcake",
        "xercamod:item_honey_cupcake",
        "xercamod:sweet_berry_cupcake",
        "xercamod:item_melon_cupcake",
        "xercamod:item_golden_cupcake",
        "xercamod:item_croissant",
        "xercamod:glowberry_cupcake",
        "xercamod:item_cocoa_cupcake",
        "xercamod:item_apple_cupcake",
        "farmersdelight:pie_crust",
        "farmersdelight:apple_pie",
    ];

    for (item of wheatDoughSwaps) {
        event.replaceInput({ output: item }, "minecraft:wheat", "create:dough");
    }

    // Redo wrap recipes
    event.remove({ output: "xercamod:item_chicken_wrap" });
    event.shapeless("2x xercamod:item_chicken_wrap", ["corn_delight:tortilla_raw", "#forge:cooked_chicken", "#forge:salad_ingredients", "farmersdelight:onion"]);
    event.remove({ output: "xercamod:item_doner_wrap" });
    event.shapeless("2x xercamod:item_doner_wrap", ["corn_delight:tortilla_raw", "xercamod:doner_slice", "#forge:salad_ingredients", "farmersdelight:onion"]);
    event.remove({ output: "farmersdelight:mutton_wrap" });
    event.shapeless("2x farmersdelight:mutton_wrap", ["corn_delight:tortilla_raw", "#forge:cooked_mutton", "#forge:salad_ingredients", "farmersdelight:onion"]);
    event.replaceInput({ output: "xercamod:item_raw_schnitzel" }, "minecraft:wheat", "create:wheat_flour");

    // Cheese slicing is now done on a cutting board
    event.remove({ output: "xercamod:cheese_slice" });
    addCuttingBoardRecipe([Item.of("xercamod:cheese_wheel")], [Ingredient.of("xercamod:cheese_slice", 4)]);
});

onEvent('tags.items', tags => {
    tags.add("farmersdelight:comfort_food", ["xercamod:oyakodon", "xercamod:beef_donburi", "xercamod:squid_ink_paella", "xercamod:glow_squid_ink_paella"]);
});

onEvent('tags.blocks', tags => {
    tags.add("farmersdelight:tray_heat_sources", ["decorative_blocks:brazier", "decorative_blocks:soul_brazier", "create:blaze_burner"]);
});