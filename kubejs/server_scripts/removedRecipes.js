onEvent('recipes', recipes => {
    for (item of global.removedItems) {
        recipes.remove({output: item});
    }

    for (item in global.replacedItems) {
        recipes.replaceInput({}, item, global.replacedItems[item]);
    }
});