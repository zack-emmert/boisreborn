onEvent('recipes', recipes => {
    recipes.shaped("1x minecraft:saddle", [
        "LLL",
        "C C",
    ], {
        L: "minecraft:leather",
        C: "minecraft:chain",
    });

    recipes.replaceInput({ type: "minecraft:crafting" }, "minecraft:ink_sac", "#forge:dyes/black");
    recipes.replaceInput({ type: "minecraft:crafting_shapeless" }, "minecraft:ink_sac", "#forge:dyes/black");
});