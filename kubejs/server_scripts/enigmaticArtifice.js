const EL_ITEMS = [
    // Tier 1
    "enigmaticlegacy:xp_scroll",
    "enigmaticlegacy:lore_inscriber",
    "enigmaticlegacy:animal_guide",
    "enigmaticlegacy:mega_sponge",

    // Tier 2
    "enigmaticlegacy:magnet_ring",
    "enigmaticlegacy:extradimensional_eye",
    "enigmaticlegacy:enchantment_transposer",
    "enigmaticlegacy:curse_transposer",
    "enigmaticlegacy:gem_ring",

    // Tier 3
    "enigmaticlegacy:ender_ring",
    "enigmaticlegacy:forbidden_axe", // Undead Exclusive
    "enigmaticlegacy:twisted_core",
    "enigmaticlegacy:dark_mirror",
    "enigmaticlegacy:hunter_guide", // Rename for grammar
    "enigmaticlegacy:mending_mixture",
    "enigmaticlegacy:ender_slayer",
    "enigmaticlegacy:infernal_shield",
    "enigmaticlegacy:recall_potion",
    "enigmaticlegacy:soul_compass",

    // Tier 4
    "enigmaticlegacy:super_magnet_ring",
    "enigmaticlegacy:etherium_ingot",
    "enigmaticlegacy:monster_charm", // Rename for grammar, Demon Exclusive
    "enigmaticlegacy:mining_charm", // Rename for grammar
    "enigmaticlegacy:cursed_scroll",
    "enigmaticlegacy:berserk_emblem",
    "enigmaticlegacy:enchanter_pearl",
    "enigmaticlegacy:avarice_scroll",
    "enigmaticlegacy:escape_scroll", // Council Exclusive
    "enigmaticlegacy:infinimeal", // Fey Exclusive
    "enigmaticlegacy:etherium_boots",
    "enigmaticlegacy:etherium_leggings",
    "enigmaticlegacy:etherium_chestplate",
    "enigmaticlegacy:etherium_helmet",
    "enigmaticlegacy:etherium_pickaxe",
    "enigmaticlegacy:etherium_axe",
    "enigmaticlegacy:etherium_shovel",
    "enigmaticlegacy:etherium_scythe",
    "enigmaticlegacy:etherium_sword",

    // Tier 5
    "enigmaticlegacy:the_twist",
    "enigmaticlegacy:antiforbidden_potion",
    "enigmaticlegacy:astral_breaker",
    "enigmaticlegacy:the_infinitum",
    "enigmaticlegacy:cosmic_heart",
    "enigmaticlegacy:oblivion_stone",
    "enigmaticlegacy:ascension_amulet",
    "enigmaticlegacy:eldritch_amulet",
    "enigmaticlegacy:desolation_ring",
];

const MANAWEAVE = {
    // Tier 1
    CIRCLE: "mna:circle",
    SQUARE: "mna:square",
    BACKSLASH: "mna:backslash",
    TRIANGLE: "mna:triangle",
    SLASH: "mna:slash",

    // Tier 2
    DIAMOND: "mna:diamond",
    KNOT: "mna:knot",
    KNOT2: "mna:knot2",
    KNOT3: "mna:knot3",
    INVERTED_TRIANGLE: "mna:inverted_triangle",
    KNOT4: "mna:knot4",

    // Tier 3
    SPLIT_TRIANGLE: "mna:split_triangle",

    // Tier 4
    STAR: "mna:star",
    HOURGLASS: "mna:hourglass",

    // Tier 5
    INFINITY: "mna:infinity",
    INVERTED_SPLIT_TRIANGLE: "mna:inverted_split_triangle",
};

const FACTONS = {
    COUNCIL: "ANCIENT_WIZARDS",
    FEY: "FEY_COURT",
    DEMONS: "DEMONS",
    UNDEAD: "UNDEAD"
};

const ELDRIN_POWERS = {
    WIND: amount => {
        return {
            affinity: "WIND",
            amount: amount,
        };
    },
    ARCANE: amount => {
        return {
            affinity: "ARCANE",
            amount: amount,
        };
    },
    EARTH: amount => {
        return {
            affinity: "EARTH",
            amount: amount,
        };
    },
    ENDER: amount => {
        return {
            affinity: "ENDER",
            amount: amount,
        };
    },
    FIRE: amount => {
        return {
            affinity: "FIRE",
            amount: amount,
        };
    },
    WATER: amount => {
        return {
            affinity: "WATER",
            amount: amount,
        };
    },
};

onEvent('recipes', recipes => {
    // output: Item, items: Ingredient[], tier: Int, patterns: string[], faction: string?
    function addManaweavingRecipe(output, items, tier, patterns, faction) {

        if (faction != null) {
            recipes.custom({
                type: "mna:manaweaving-recipe",
                tier: tier,
                output: output,
                items: items,
                patterns: patterns,
                requiredFaction: faction,
            });
        } else {
            recipes.custom({
                type: "mna:manaweaving-recipe",
                tier: tier,
                output: output,
                items: items,
                patterns: patterns,
            });
        }

    }
    // output: Item, items: Ingredient[], tier: Int, power_requirements: {affinity: string, amount: Int}[], colors: Int[], faction: string?
    function addEldrinAltarRecipe(output, items, tier, power_requirements, colors, faction) {
        if (faction != null) {
            recipes.custom({
                type: "mna:eldrin-altar",
                output: output,
                tier: tier,
                items: items,
                power_requirements: power_requirements,
                colors: colors,
                faction: faction,
            });
        } else {
            recipes.custom({
                type: "mna:eldrin-altar",
                output: output,
                tier: tier,
                items: items,
                power_requirements: power_requirements,
                colors: colors
            });
        }
    }

    for (item of EL_ITEMS) {
        recipes.remove({ output: item });
    }

    // |===| Tier 1 Items |===|
    addManaweavingRecipe("enigmaticlegacy:xp_scroll", [
        "forge:gems/emerald",
        "mna:dusts/vinteum",
        "mna:vellum",
    ], 1, [
        MANAWEAVE.SQUARE,
        MANAWEAVE.CIRCLE,
    ]);

    addManaweavingRecipe("enigmaticlegacy:lore_inscriber", [
        "mna:arcanist_ink",
        "mna:animated_quill",
    ], 1, [
        MANAWEAVE.TRIANGLE,
    ]);

    addManaweavingRecipe("enigmaticlegacy:animal_guide", [
        "forge:raw_fishes",
        "forge:seeds",
        "forge:bones",
        "minecraft:book",
    ], 1, [
        MANAWEAVE.TRIANGLE,
        MANAWEAVE.SQUARE,
    ]);

    addManaweavingRecipe("enigmaticlegacy:mega_sponge", [
        "minecraft:sponge",
        "minecraft:nautilus_shell",
        "minecraft:heart_of_the_sea",
        "mna:dusts/vinteum",
    ], 1, [
        MANAWEAVE.CIRCLE
    ]);

    // |===| Tier 2 Items |===|
    addManaweavingRecipe("enigmaticlegacy:magnet_ring", [
        "mna:mundane_ring",
        "forge:ingots/iron",
        "forge:dusts/redstone",
    ], 2, [
        MANAWEAVE.DIAMOND,
        MANAWEAVE.SQUARE,
    ]);

    addManaweavingRecipe("enigmaticlegacy:extradimensional_eye", [
        "forge:ingots/gold",
        "mna:mote_arcane",
        "minecraft:ender_eye",
        "minecraft:phantom_membrane",
        "minecraft:blaze_powder",
    ], 2, [
        MANAWEAVE.KNOT,
        MANAWEAVE.INVERTED_TRIANGLE,
        MANAWEAVE.KNOT4,
    ]);

    addManaweavingRecipe("enigmaticlegacy:enchantment_transposer", [
        "minecraft:book",
        "mna:mote_ender",
        "mna:mote_arcane",
        "forge:nuggets/gold",
        "forge:dusts/redstone",
    ], 2, [
        MANAWEAVE.SLASH,
        MANAWEAVE.SQUARE,
        MANAWEAVE.BACKSLASH,
    ]);

    addManaweavingRecipe("enigmaticlegacy:curse_transposer", [
        "enigmaticlegacy:enchantment_transposer",
        "enigmaticlegacy:evil_essence",
    ], 2, [
        MANAWEAVE.SLASH,
        MANAWEAVE.SQUARE,
        MANAWEAVE.BACKSLASH,
    ]);

    addManaweavingRecipe("enigmaticlegacy:gem_ring", [
        "mna:mundane_ring",
        "forge:ingots/gold",
        "forge:nuggets/gold",
    ], 2, [
        MANAWEAVE.DIAMOND,
        MANAWEAVE.CIRCLE,
        MANAWEAVE.DIAMOND,
    ]);

    // |===| Tier 3 Items |===|
    addManaweavingRecipe("enigmaticlegacy:ender_ring", [
        "mna:mundane_ring",
        "mna:mote_ender",
        "minecraft:ender_chest",
        "mna:rune_ender",
    ], 3, [
        MANAWEAVE.SQUARE,
        MANAWEAVE.SPLIT_TRIANGLE,
        MANAWEAVE.SQUARE,
    ]);

    addManaweavingRecipe("enigmaticlegacy:forbidden_axe", [
        "minecraft:diamond_axe",
        "minecraft:wither_skeleton_skull",
        "mna:mote_fire",
        "mna:mote_ender",
    ], 3, [
        MANAWEAVE.KNOT,
        MANAWEAVE.SQUARE,
        MANAWEAVE.DIAMOND,
        MANAWEAVE.SQUARE,
        MANAWEAVE.KNOT4,
    ], FACTONS.UNDEAD);

    addManaweavingRecipe("enigmaticlegacy:twisted_core", [
        "enigmaticlegacy:earth_heart",
        "mna:mote_ender",
        "mna:bone_ash",
    ], 3, [
        MANAWEAVE.SPLIT_TRIANGLE,
        MANAWEAVE.INVERTED_TRIANGLE,
        MANAWEAVE.SPLIT_TRIANGLE,
    ]);

    addManaweavingRecipe("enigmaticlegacy:dark_mirror", [
        "forge:glass_panes/colorless",
        "enigmaticlegacy:twisted_core",
        "mna:arcane_ash",
        "mna:vinteum_ingot",
    ], 3, [
        MANAWEAVE.INVERTED_TRIANGLE,
        MANAWEAVE.CIRCLE,
        MANAWEAVE.INVERTED_TRIANGLE,
    ]);

    addManaweavingRecipe("enigmaticlegacy:hunter_guide", [
        "minecraft:book",
        "minecraft:bone",
        "forge:raw_chicken",
        "forge:raw_fishes",
        "minecraft:lead",
    ], 3, [
        MANAWEAVE.SQUARE,
        MANAWEAVE.CIRCLE,
        MANAWEAVE.SQUARE,
    ]);

    addManaweavingRecipe("enigmaticlegacy:mending_mixture", [
        "quark:bottled_cloud",
        "mna:mote_arcane",
        "mna:purified_vinteum_dust",
        "minecraft:phantom_membrane",
        "forge:gems/prismarine",
    ], 3, [
        MANAWEAVE.KNOT,
        MANAWEAVE.KNOT2,
        MANAWEAVE.KNOT3,
        MANAWEAVE.KNOT4,
    ]);

    addManaweavingRecipe("enigmaticlegacy:ender_slayer", [
        "minecraft:diamond_sword",
        "enigmaticlegacy:evil_essence",
        "enigmaticlegacy:evil_essence",
        "mna:mote_ender",
        "mna:rune_ender",
    ], 3, [
        MANAWEAVE.SLASH,
        MANAWEAVE.DIAMOND,
        MANAWEAVE.KNOT,
        MANAWEAVE.BACKSLASH,
    ]);

    addManaweavingRecipe("enigmaticlegacy:infernal_shield", [
        "minecraft:shield",
        "mna:mote_fire",
        "mna:mote_fire",
        "forge:ingots/netherite",
        "enigmaticlegacy:twisted_core",
    ], 3, [
        MANAWEAVE.SQUARE,
        MANAWEAVE.CIRCLE,
        MANAWEAVE.DIAMOND,
        MANAWEAVE.INVERTED_TRIANGLE,
    ]);

    addManaweavingRecipe("enigmaticlegacy:recall_potion", [
        "minecraft:honey_bottle",
        "minecraft:nether_wart",
        "mna:purified_vinteum_dust",
        "occultism:iesnium_dust",
    ], 3, [
        MANAWEAVE.SPLIT_TRIANGLE,
        MANAWEAVE.CIRCLE,
        MANAWEAVE.SPLIT_TRIANGLE,
    ]);

    addManaweavingRecipe("enigmaticlegacy:soul_compass", [
        "minecraft:compass",
        "minecraft:soul_lantern",
        "mna:mote_fire",
        "forge:ingots/netherite",
        "enigmaticlegacy:twisted_core",
    ], 3, [
        MANAWEAVE.SQUARE,
        MANAWEAVE.CIRCLE,
        MANAWEAVE.DIAMOND,
        MANAWEAVE.INVERTED_TRIANGLE,
    ]);

    // |===| Tier 4 Items |===|
    addManaweavingRecipe("enigmaticlegacy:super_magnet_ring", [
        "enigmaticlegacy:magnet_ring",
        "mna:greater_mote_ender",
        "mna:purified_vinteum_dust",
    ], 4, [
        MANAWEAVE.STAR,
        MANAWEAVE.DIAMOND,
        MANAWEAVE.SQUARE,
    ]);

    addEldrinAltarRecipe("enigmaticlegacy:etherium_ingot", [
        "minecraft:netherite_ingot",
        "occultism:iesnium_ingot"
    ], 4, [
        ELDRIN_POWERS.ARCANE(15),
    ], [
        5887445,
        16579836,
    ]);

    addEldrinAltarRecipe("enigmaticlegacy:monster_charm", [
        "minecraft:skeleton_skull",
        "minecraft:soul_lantern",
        "mna:greater_mote_fire",
        "mna:greater_mote_ender",
    ], 4, [
        ELDRIN_POWERS.FIRE(10),
        ELDRIN_POWERS.ENDER(5),
    ], [
        16742144,
        5651548,
    ], FACTONS.DEMONS);

    addEldrinAltarRecipe("enigmaticlegacy:mining_charm", [
        "enigmaticlegacy:earth_heart",
        "forge:ingots/netherite",
        "forge:gems/diamond",
        "forge:gems/emerald",
        "forge:ingots/gold",
        "forge:ingots/silver",
        "forge:ingots/iron",
        "forge:ingots/copper",
    ], 4, [
        ELDRIN_POWERS.EARTH(10),
    ], [
        9539205,
        6383210,
    ]);

    addEldrinAltarRecipe("enigmaticlegacy:cursed_scroll", [
        "enigmaticlegacy:darkest_scroll",
        "enigmaticlegacy:twisted_core",
        "occultism:otherworld_ashes",
        "occultism:afrit_essence",
    ], 4, [
        ELDRIN_POWERS.ARCANE(5),
        ELDRIN_POWERS.ENDER(10),
    ], [
        0,
        10753824,
    ]);

    addEldrinAltarRecipe("enigmaticlegacy:berserk_emblem", [
        "enigmaticlegacy:twisted_core",
        "forge:ingots/netherite",
        "minecraft:crimson_fungus",
        "mna:living_flame",
    ], 4, [
        ELDRIN_POWERS.FIRE(15),
    ], [
        16742144,
        11734536,
    ]);

    addEldrinAltarRecipe("enigmaticlegacy:enchanter_pearl", [
        "minecraft:ender_pearl",
        "enigmaticlegacy:evil_essence",
        "enigmaticlegacy:evil_essence",
        "mna:rune_arcane",
    ], 4, [
        ELDRIN_POWERS.ARCANE(10),
        ELDRIN_POWERS.ENDER(10),
    ], [
        24477,
        4268367,
    ]);

    addEldrinAltarRecipe("enigmaticlegacy:avarice_scroll", [
        "enigmaticlegacy:darkest_scroll",
        "enigmaticlegacy:twisted_core",
        "enigmaticlegacy:gem_ring",
        "mna:chimerite_gem",
        "forge:gems/emerald",
        "forge:gems/diamond",
    ], 4, [
        ELDRIN_POWERS.FIRE(5),
        ELDRIN_POWERS.ENDER(15),
    ], [
        16742144,
        5651548,
    ]);

    addEldrinAltarRecipe("enigmaticlegacy:escape_scroll", [
        "mna:vellum",
        "occultism:spirit_attuned_gem",
        "minecraft:ender_pearl",
        "occultism:iesnium_dust",
        "occultism:iesnium_dust",
    ], 4, [
        ELDRIN_POWERS.ARCANE(15),
        ELDRIN_POWERS.WIND(5),
    ], [
        24477,
        13758969,
    ], FACTONS.COUNCIL);

    addEldrinAltarRecipe("enigmaticlegacy:infinimeal", [
        "enigmaticlegacy:earth_heart",
        "minecraft:small_flowers",
        "forge:fruits",
        "forge:grain/wheat",
        "minecraft:nether_wart",
        "minecraft:warped_fungus",
        "minecraft:weeping_vines",
        "minecraft:cocoa_beans",
    ], 4, [
        ELDRIN_POWERS.WATER(10),
        ELDRIN_POWERS.EARTH(10)
    ], [
        11085,
        21032,
    ], FACTONS.FEY);

    function addEtheriumItemRecipe(output, baseItem) {
        addEldrinAltarRecipe(output, [
            baseItem,
            "enigmaticlegacy:etherium_ingot",
            "enigmaticlegacy:etherium_ingot",
        ], 4, [
            ELDRIN_POWERS.ARCANE(15),
            ELDRIN_POWERS.WIND(5),
        ], [
            5887445,
            16579836,
        ]);
    }

    addEtheriumItemRecipe("enigmaticlegacy:etherium_boots", "minecraft:netherite_boots");
    addEtheriumItemRecipe("enigmaticlegacy:etherium_leggings", "minecraft:netherite_leggings");
    addEtheriumItemRecipe("enigmaticlegacy:etherium_chestplate", "minecraft:netherite_chestplate");
    addEtheriumItemRecipe("enigmaticlegacy:etherium_helmet", "minecraft:netherite_helmet");
    addEtheriumItemRecipe("enigmaticlegacy:etherium_pickaxe", "minecraft:netherite_pickaxe");
    addEtheriumItemRecipe("enigmaticlegacy:etherium_axe", "minecraft:netherite_axe");
    addEtheriumItemRecipe("enigmaticlegacy:etherium_shovel", "minecraft:netherite_shovel");
    addEtheriumItemRecipe("enigmaticlegacy:etherium_scythe", "minecraft:netherite_hoe");
    addEtheriumItemRecipe("enigmaticlegacy:etherium_sword", "minecraft:netherite_sword");

    // |===| Tier 5 Items |===|
    addManaweavingRecipe("enigmaticlegacy:astral_dust", [
        "occultism:iesnium_dust",
        "mna:purified_vinteum_dust",
        "forge:dusts/redstone",
        "forge:dusts/glowstone",
    ], 5, [
        MANAWEAVE.HOURGLASS,
        MANAWEAVE.INFINITY,
    ]);

    addEldrinAltarRecipe("enigmaticlegacy:the_twist", [
        "enigmaticlegacy:the_acknowledgment",
        "forge:ingots/netherite",
        "forge:ingots/netherite",
        "enigmaticlegacy:evil_essence",
        "enigmaticlegacy:evil_essence",
        "enigmaticlegacy:evil_essence",
        "enigmaticlegacy:evil_essence",
        "enigmaticlegacy:twisted_core",
    ], 5, [
        ELDRIN_POWERS.ENDER(30),
        ELDRIN_POWERS.FIRE(10),
    ], [
        4999244,
        7405568,
    ]);

    addEldrinAltarRecipe("enigmaticlegacy:antiforbidden_potion", [
        "minecraft:honey_bottle",
        "enigmaticlegacy:astral_fruit",
        "enigmaticlegacy:earth_heart",
        "enigmaticlegacy:cosmic_heart",
        "mna:greater_mote_arcane",
        "mna:living_flame",
        "occultism:iesnium_dust",
    ], 5, [
        ELDRIN_POWERS.ARCANE(30),
        ELDRIN_POWERS.EARTH(30),
        ELDRIN_POWERS.FIRE(30),
        ELDRIN_POWERS.WATER(30),
        ELDRIN_POWERS.WIND(30),
    ], [
        16579836,
        16757834,
    ]);

    addEldrinAltarRecipe("enigmaticlegacy:astral_breaker", [
        "mna:greater_mote_arcane",
        "enigmaticlegacy:etherium_pickaxe",
        "enigmaticlegacy:etherium_axe",
        "enigmaticlegacy:etherium_shovel",
        "enigmaticlegacy:astral_dust",
        "enigmaticlegacy:astral_dust",
        "enigmaticlegacy:astral_dust",
        "enigmaticlegacy:astral_dust",
    ], 5, [
        ELDRIN_POWERS.ARCANE(20),
        ELDRIN_POWERS.EARTH(5),
    ], [
        5887445,
        3487801,
    ]);

    addEldrinAltarRecipe("enigmaticlegacy:the_infinitum", [
        "enigmaticlegacy:the_twist",
        "enigmaticlegacy:cosmic_heart",
        "enigmaticlegacy:evil_essence",
        "enigmaticlegacy:evil_essence",
        "enigmaticlegacy:enchanter_pearl",
        "enigmaticlegacy:abyssal_heart",
        "forge:ingots/netherite",
        "forge:ingots/netherite",
    ], 5, [
        ELDRIN_POWERS.ENDER(35),
        ELDRIN_POWERS.ARCANE(10),
    ], [
        3681612,
        1448732,
    ]);

    addEldrinAltarRecipe("enigmaticlegacy:cosmic_heart", [
        "minecraft:heart_of_the_sea",
        "minecraft:nether_star",
        "enigmaticlegacy:astral_dust",
        "enigmaticlegacy:astral_dust",
        "enigmaticlegacy:astral_dust",
        "enigmaticlegacy:astral_dust",
    ], 5, [
        ELDRIN_POWERS.ARCANE(40),
    ], [
        24477,
        6697585,
    ]);

    addEldrinAltarRecipe("enigmaticlegacy:oblivion_stone", [
        "mna:eldrin_rift",
        "mna:greater_mote_ender",
        "mna:greater_mote_ender",
        "occultism:dimensional_matrix",
    ], 5, [
        ELDRIN_POWERS.ENDER(30),
        ELDRIN_POWERS.ARCANE(5)
    ], [
        381880,
        24477,
    ]);

    addEldrinAltarRecipe('enigmaticlegacy:ascension_amulet', [
        "enigmaticlegacy:enigmatic_amulet",
        "enigmaticlegacy:astral_dust",
        "minecraft:amethyst_shard",
        "enigmaticlegacy:etherium_ingot",
        "minecraft:dragon_breath",

    ], 5, [
        ELDRIN_POWERS.ARCANE(30)
    ], [
        24477,
        6697585,
    ]);

    addEldrinAltarRecipe("enigmaticlegacy:eldritch_amulet", [
        "enigmaticlegacy:ascension_amulet",
        "enigmaticlegacy:abyssal_heart",
        "enigmaticlegaxy:evil_essence",
        "minecraft:nether_star",
        "minecraft:netherite_ingot",
        "enigmaticlegacy:twisted_heart"
    ], 5, [
        ELDRIN_POWERS.ENDER(30),
        ELDRIN_POWERS.ARCANE(5)
    ], [
        381880,
        24477,
    ]);

    addEldrinAltarRecipe("enigmaticlegacy:desolation_ring", [
        "enigmaticlegacy:gem_ring",
        "enigmaticlegacy:abyssal_heart",
        "enigmaticlegacy:cosmic_heart",
        "enigmaticlegaxy:evil_essence",
        "minecraft:netherite_ingot",
        "enigmaticlegacy:twisted_heart"
    ], 5, [
        ELDRIN_POWERS.ENDER(30),
        ELDRIN_POWERS.ARCANE(5)
    ], [
        381880,
        24477,
    ]);
});

const ODIN_LOOT_ITEMS = [
    "enigmaticlegacy:golem_heart",
    "enigmaticlegacy:angel_blessing",
    "enigmaticlegacy:ocean_stone",
    "enigmaticlegacy:magma_heart",
    "enigmaticlegacy:eye_of_nebula",
    "enigmaticlegacy:void_pearl",
    "enigmaticlegacy:unwitnessed_amulet",
    "enigmaticlegacy:guardian_heart",
    "enigmaticlegacy:astral_fruit",
];

onEvent("lootjs", lootjs => {
    lootjs.addEntityLootModifier("mna:odin")
        .randomChanceWithLooting(0.5, 1.3)
        .killedByPlayer()
        .thenAddWeighted(ODIN_LOOT_ITEMS);

    lootjs.addEntityLootModifier("mna:odin")
        .killedByPlayer()
        .playerPredicate(player => {
            player.stats.get("minecraft:play_time_with_seven_curses")
                / player.stats.get("minecraft:time_played")
                >= 0.95
        })
        .thenAdd("enigmaticlegacy:cursed_stone");
});

onEvent("tags.items", tags => {
    // They're amulets. They belong in the amulet slot
    let amulets = ["enigmaticlegacy:enigmatic_amulet", "enigmaticlegacy:unwitnessed_amulet", "enigmaticlegacy:ascension_amulet"];
    tags.remove("curios:charm", amulets);
    tags.add("curios:necklace", amulets);
})