const TOME = Item.of('akashictome:tome', 
    '{"akashictome:data":{adorn:{Count:1b,id:"adorn:guide_book"},adorn_0:{Count:1b,id:"adorn:traders_manual",tag:{"akashictome:definedMod":"adorn_0"}},alexsmobs:{Count:1b,id:"alexsmobs:animal_dictionary",tag:{"akashictome:displayName":{text:\'{"translate":"item.alexsmobs.animal_dictionary"}\'},"akashictome:is_morphing":1b,display:{Name:\'{"translate":"akashictome.sudo_name","with":[{"color":"green","translate":"item.alexsmobs.animal_dictionary"}]}\'}}},blue_skies:{Count:1b,id:"blue_skies:blue_journal",tag:{"akashictome:displayName":{text:\'{"translate":"item.blue_skies.blue_journal"}\'},"akashictome:is_morphing":1b,display:{Name:\'{"translate":"akashictome.sudo_name","with":[{"color":"green","translate":"item.blue_skies.blue_journal"}]}\'}}},byg:{Count:1b,id:"byg:biomepedia",tag:{"akashictome:displayName":{text:\'{"translate":"item.byg.biomepedia"}\'},"akashictome:is_morphing":1b,display:{Name:\'{"translate":"akashictome.sudo_name","with":[{"color":"green","translate":"item.byg.biomepedia"}]}\'}}},enigmaticlegacy:{Count:1b,id:"enigmaticlegacy:the_acknowledgment",tag:{"akashictome:displayName":{text:\'{"translate":"item.enigmaticlegacy.the_acknowledgment"}\'},"akashictome:is_morphing":1b,display:{Name:\'{"translate":"akashictome.sudo_name","with":[{"color":"green","translate":"item.enigmaticlegacy.the_acknowledgment"}]}\'}}},immersiveengineering:{Count:1b,id:"immersiveengineering:manual",tag:{"akashictome:displayName":{text:\'{"translate":"item.immersiveengineering.manual"}\'},"akashictome:is_morphing":1b,display:{Name:\'{"translate":"akashictome.sudo_name","with":[{"color":"green","translate":"item.immersiveengineering.manual"}]}\'}}},mna:{Count:1b,id:"mna:guide_book",tag:{"akashictome:displayName":{text:\'{"translate":"item.mna.guide_book"}\'},"akashictome:is_morphing":1b,display:{Name:\'{"translate":"akashictome.sudo_name","with":[{"color":"green","translate":"item.mna.guide_book"}]}\'}}},occultism:{Count:1b,id:"occultism:dictionary_of_spirits"},patchouli:{Count:1b,id:"patchouli:guide_book",tag:{"patchouli:book":"engineersdecor:engineersdecor_manual"}},patchouli_0:{Count:1b,id:"patchouli:guide_book",tag:{"akashictome:definedMod":"patchouli_0","akashictome:displayName":{text:\'{"translate":"book.apotheosis.name"}\'},"akashictome:is_morphing":1b,display:{Name:\'{"translate":"akashictome.sudo_name","with":[{"color":"green","translate":"book.apotheosis.name"}]}\'},"patchouli:book":"apotheosis:apoth_chronicle"}},tconstruct:{Count:1b,id:"tconstruct:materials_and_you"}},"akashictome:is_morphing":1b}'
);

const STARTING_ITEMS = [
    TOME,
];

onEvent('player.logged_in', event => {
    if (!event.player.stages.has("boisreborn.starting_items")) {
        event.player.stages.add("boisreborn.starting_items");
        event.player.inventory.clear();

        for (item of STARTING_ITEMS) {
            event.player.give(item);
        }
    }
});

onEvent('recipes', recipes => {
    recipes.shaped(TOME, [
        'LB',
        'BB',
    ], {
        L: "#forge:leather",
        B: "minecraft:book",
    });
});