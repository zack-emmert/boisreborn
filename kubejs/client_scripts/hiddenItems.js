onEvent('jei.hide.items', jei => {
    for (item of global.removedItems) {
        jei.hide(item);
    }

    for (item in global.replacedItems) {
        jei.hide(item);
    }
});